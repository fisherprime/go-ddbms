A distributed database middleware.

# Database source

[Sample database](https://github.com/datacharmer/test_db)

# Setup

** All paths reference the source directory; the cloned repo's directory **

1. Clone to your [GOPATH](https://github.com/golang/go/wiki/GOPATH).

1. Install packages:

   - libvirt
   - nfs-utils
   - vagrant

1. Ensure the libvirt service is up

```sh
systemctl restart libvirtd.service
```

1. Setup vagrant boxes (setting up the NFS share will require superuser permissions):

```sh
cd vagrant; vagrant up
```

1. Build middleware:

```sh
make
```

1. Execute middleware:

```sh
./build/ddbms/ddbms
```

** Vagrant up may fail to authenticate SSH, if this happens, note the VM that failed to setup &
retry until it does **

** VM user: vagrant, pass: vagrant **

# Fragments schemas (as the implemented structs)

```go
FragmentDets{
	filters:     [][]string{{""}},
	foreignKeys: "",
	globalTable: "departments",
	name:        "departments1",
	pKey:        "dept_no",
	projection:  "dept_no, dept_name",
	size:        20,
}
```

# API usage

- Link: [127.0.2.2:2000/api/query](127.0.2.2:2000/api/query)
- Request:

      	- Type: POST
      	- Body type: form / form-data
      	- Key: query
      	- Value: <amazing query>

# Issues

1. Fragments that join to another fragment are **completely** ignored in the reduction phase when
   they return a NULL result.
   Rather than define the final response as NULL (assuming joins), the non-NULL fragments are joined
   and returned.

# Warnings

1. Do not query all fields from the: salaries, employees tables on postman
   (v7.2.0); the response is large enough to make it freeze up
