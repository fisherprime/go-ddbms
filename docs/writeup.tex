\newcommand*{\defaultincludepath}{include}

\newcommand*{\gocodepath}{../backend}
\newcommand*{\vagrantroot}{../vagrant}
\newcommand*{\ddbmsfilespath}{\vagrantroot/ddbms}

\newcommand*{\ddbmsdatapath}{\ddbmsfilespath/data}
\newcommand*{\shellcodepath}{\ddbmsfilespath/shell}
\newcommand*{\sqlcodepath}{\ddbmsfilespath/sql}

\input{\defaultincludepath/Report.tex}
\input{\defaultincludepath/Code.tex}

\usepackage{listings-golang}

\begin{document}
	\pagenumbering{gobble}
	\begin{titlepage}
		\centering
		\scshape
		\Large
			\vspace*{10cm}
			\textbf{go-ddbms: A Distributed Database Management System} \\[2cm]

			Implementation writeup

		\vspace*{\fill}

	\thispagestyle{empty}
	\end{titlepage}

	\pagenumbering{roman}
	\tableofcontents
	\newpage

    \pagenumbering{arabic}
	\rfoot{Page\ \thepage\ of\ \protect\pageref{LastPage}}

	\begin{abstract}
		This document presents the writeup for a distributed database
		management system implementation hosted at:
		\url{https://gitlab.com/fisherprime/go-ddbms}.
		3 database management systems are used to implement this system:
		PostgreSQL, MariaDB \& CockroachDB.
		This system is capable of parsing valid SELECT \& UPDATE queries to view \&
		manipulate the information stored in the system's table fragments. \\
		\end{abstract}
	\newpage

	\chapter{Virtual Machines}
		\section{Setup}

		3 vagrant-managed virtual machines (VMs) are implemented from base
		debian/stretch64 \& centos/7 boxes runnning on libvirt, each hosting
		their own database management system (DBMS).
		A Vagrantfile is provided in the vagrant directory that manages the
		setup and provisioning of the VM's: resources, port forwaring and
		configuration to allow remote administration of their respective
		DBMSs. \\

		The naming of the virtual machines is as follows:
		\begin{enumerate}[label = \roman* )]
			\item VM1: ddbms\_postgres;
			\item VM2: ddbms\_mariadb; and
			\item VM3: ddbms\_cockroach. \\
		\end{enumerate}
		As is evident from the VM names, they are named according to the DBMS they
		have set up. \\

		To setup the VMs the following commands need to be executed:
		\begin{lstlisting}[language=sh]
		systemctl restart libvirtd.service
		cd vagrant
		vagrant up
		\end{lstlisting}
	\newpage

	\chapter{Database}

		The database used is a publicly available sample sourced from:
		\url{https://github.com/datacharmer/test_db}.
		The database was trimmed to represent 100000 employees. \\

		Horizontal fragmentation was performed on the: dept\_emp,
		employees \& salaries tables: given no user application, they were split
		to have upto an upper limit of records as follows:
		\begin{enumerate}[label = \roman* )]
			\item dept\_emp: 35,000;
			\item employees: 45,000; and
			\item salaries: 200,000. \\
		\end{enumerate}

		Vertical fragmentation was performed on the dept\_manager table, the
		assumed user application being:
		\begin{enumerate}[label = \roman* )]
			\item select emp\_no, dept\_no from dept\_manager. \\
		\end{enumerate}

		The resultant fragments are hosted accross the various sites in the
		following scheme:
		\begin{enumerate}[label = \roman* )]
			\item ddbms\_postgres: departments1, dept\_emp1, dept\_emp2,
				\ldots, dept\_emp4 \& dept\_manager1;
			\item ddbms\_mariadb: employees1, employees2 \& employees3; and
			\item ddbms\_cockroach: salaries1, salaries2, \ldots \& salaries5. \\
		\end{enumerate}

	\chapter{Middleware}
		\section{Lexer}

		The lexer was adapted from the sql-parser project at:
		\url{https://github.com/sh4t/sql-parser}, which was inspured by Rob Pike's
		lecture on lexical scanning in Go, located at:
		\url{https://www.youtube.com/watch?v=HxaD_trXwRE}. \\

		Support for asterisks ($*$) selections was added: (dept\_emp.$*$) \&
		code sanitized. \\

		\section{Parser}

		The implemented parser receives token passed to it by the lexer's
		channel \& groups them into: (projection, cartesian product \&
		selection) sections for SELECT queries.
		For the case of UPDATE queries, the sections are (tables, values,
		filters), the selection, filters \& values sections are implemented as
		slices of string slices. \\

		\section{Reducer}

		The implemented reducer selects the appropriate fragments to query as
		defined in the fragment schema, which is defined as:
		\lstinputlisting[firstline=19, lastline=27]{"\gocodepath/db.go"} % chktex 18

		Fragments that will result in NULL output on applying filters are omitted
		from the query process.
		The reducer operating in this mode has the side effect of emitting
		fragments essential in the query process (when joining); a query that
		would have resulted in a final NULL response, is instead a join of the
		non-NULL fragments from this step. \\

		\section{Optimizer}

		The implemented optimizer uses Heap's algorithm to generate all possible
		permutations for the fragment joins, the most optimal assumption is then
		calculated using the fragment sizes \& parent-child relationship.
		Largest possible size for a join between a parent \& child table is
		the size of the child table.
		No estimation is based on the fraction of parent data specified by a
		filter. \\

		The Heap's algorithm implementation is as follows:
		\lstinputlisting[firstline=195, lastline=218]{"\gocodepath/optimizer.go"} % chktex 18

		\section{Querying}

		The optimal querying order is passed to the querying function \&
		records are obtained from the fragments \& shipped to the next.
		Temporary tables are created of the form
		``<previous-fragment-name>\_temp''at the site with the next fragment
		to query \& are populated. \\

		An SQL query joiner merges 2 SELECT queries wit a UNION or implied
		join for horizontal \& vertical fragments respectively.
		This merged query forms the statement that will request the necessary
		records from the next site.
		Once the records are retrieved, the  temporary table is then deleted,
		the new fragment response forming the data that will be shipped to the
		next node. \\

		\section{Response Parser}

		The parsing of the results involves the conversion of an *sql.Rows
		item into a JSON representation returned as part of the response on
		success.
		An implementation that uses reflection to type the column values was
		sourced from: \\
		\url{https://stackoverflow.com/questions/42774467/how-to-convert-sql-rows-to-typed-json-in-golang}
		and used as is. \\

		A request for the query ``select salaries.emp\_no, salaries.salary,
		dept\_manager.* from salaries,dept\_manager where
		salaries.emp\_no=10001 and from\_date$<$1990-06-24;'' yields the
		following
		result\footnote{response has been word-wrapped}:
		\begin{verbatim}
{
	"data":
	"{\"emp_no\":[10001,10001,10001,10001],\"salary\":[60117,62102,66074,66596]}",
    "failure": "",
    "response": "Success"
}

		\end{verbatim}

		On failure during the: lexing, parsing, querying, \ldots steps, the API
		response is of the form\footnote{response has been word-wrapped}:
		\begin{verbatim}
{
    "data": "",
	"failure": "Error 1064: You have an error in your SQL syntax; check the
	manual that corresponds to your MariaDB server version for the right
	syntax to use near 'and from_date<'1995-06-24'' at line 1",
    "response": ""
}
		\end{verbatim}

		Where the failure field stores the appropriate error message for the
		failure. \\

		\section{API}

		The endpoint is bound to \url{http://127.0.2.2:2000/api/query} upon starting
		up the middleware, to  query the system, the following configuration
		needs to be observed:
		\begin{enumerate}[label = \roman* )]
			\item Request type: POST;
			\item Body type: form / form-data;
			\item Key: "query"; and
			\item Value as the query to be run. \\
		\end{enumerate}

		It is not recommended to query everything from the: horizontally
		fragmented tables for the reason the data returned is too much for
		some consumers (Postman v7.2.0).
\end{document}
