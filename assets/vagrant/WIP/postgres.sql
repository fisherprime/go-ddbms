/**
 * Mysql wrapper
 */
CREATE SERVER mysql
     FOREIGN DATA WRAPPER mysql_fdw
     OPTIONS (user 'mysql', host '127.0.0.1', port '3306', database "ddbms-mariadb");

CREATE USER MAPPING FOR postgres
SERVER mysql
OPTIONS (username 'mysql', password 'ddbms');

CREATE TABLE node2 (
	node_id int primary key,
	location text
)
ENGINE=FEDERATED
DEFAULT CHARSET=UTF-8
CONNECTION='mysql/node2';

CREATE TABLE metrics1 (
    metrics_id int primary key,
	node_id int,
	time timestamp,
	moisture real,
	humidity real,
	temperature real,
	light real
)
ENGINE=FEDERATED
DEFAULT CHARSET=UTF-8
CONNECTION='mysql/metrics1';

/**
 * CockroachDB wrapper
 */
-- TODO complete
CREATE SERVER mysql
     FOREIGN DATA WRAPPER cockroach
     OPTIONS (user 'mysql', host '127.0.0.1', port '3306', database "ddbms-mariadb");

CREATE USER MAPPING FOR postgres
SERVER mysql
OPTIONS (username 'cockroach', password 'ddbms');


CREATE TABLE metrics2 (
    metrics_id int primary key,
	node_id int,
	time timestamp,
	moisture real,
	humidity real,
	temperature real,
	light real
)
ENGINE=FEDERATED
DEFAULT CHARSET=UTF-8
CONNECTION='cockroach/metrics2';
