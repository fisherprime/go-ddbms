DROP TABLE IF EXISTS departments1;
DROP TABLE IF EXISTS dept_manager1;
DROP TABLE IF EXISTS dept_emp1;
DROP TABLE IF EXISTS dept_emp2;
DROP TABLE IF EXISTS dept_emp3;
DROP TABLE IF EXISTS dept_emp4;

CREATE TABLE departments1 (
    dept_no   CHAR(4)     PRIMARY KEY,
    dept_name VARCHAR(40) NOT NULL UNIQUE
);

CREATE TABLE dept_manager1 (
   emp_no    INTEGER PRIMARY KEY,
   dept_no   CHAR(4) NOT NULL
); 

CREATE TABLE dept_emp1 (
    emp_no    INTEGER NOT NULL,
    dept_no   CHAR(4) NOT NULL,
    from_date DATE    NOT NULL,
    to_date   DATE    NOT NULL
);

CREATE TABLE dept_emp2 (
    emp_no    INTEGER NOT NULL,
    dept_no   CHAR(4) NOT NULL,
    from_date DATE    NOT NULL,
    to_date   DATE    NOT NULL
);

CREATE TABLE dept_emp3 (
    emp_no    INTEGER NOT NULL,
    dept_no   CHAR(4) NOT NULL,
    from_date DATE    NOT NULL,
    to_date   DATE    NOT NULL
);

CREATE TABLE dept_emp4 (
    emp_no    INTEGER NOT NULL,
    dept_no   CHAR(4) NOT NULL,
    from_date DATE    NOT NULL,
    to_date   DATE    NOT NULL
);

copy departments1 from '/vagrant_data/data/departments1.csv' csv;

copy dept_manager1 from '/vagrant_data/data/dept_manager1.csv' csv;

copy dept_emp1 from '/vagrant_data/data/dept_emp1.csv' csv;
copy dept_emp2 from '/vagrant_data/data/dept_emp2.csv' csv;
copy dept_emp3 from '/vagrant_data/data/dept_emp3.csv' csv;
copy dept_emp4 from '/vagrant_data/data/dept_emp4.csv' csv;

ALTER TABLE departments1 OWNER TO ddbms;
ALTER TABLE dept_manager1 OWNER TO ddbms;
ALTER TABLE dept_emp1 OWNER TO ddbms;
ALTER TABLE dept_emp2 OWNER TO ddbms;
ALTER TABLE dept_emp3 OWNER TO ddbms;
