DROP USER IF EXISTS 'ddbms'@'localhost';
DROP USER IF EXISTS 'ddbms'@'%';
FLUSH PRIVILEGES;

CREATE USER 'ddbms'@'localhost' IDENTIFIED BY 'ddbms';
CREATE USER 'ddbms'@'%' IDENTIFIED BY 'ddbms';
CREATE DATABASE IF NOT EXISTS ddbms_mariadb;

GRANT ALL PRIVILEGES ON ddbms_mariadb.* TO 'ddbms'@'localhost';
GRANT ALL PRIVILEGES ON ddbms_mariadb.* TO 'ddbms'@'%';
