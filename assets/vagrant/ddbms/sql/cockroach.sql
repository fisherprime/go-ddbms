DROP TABLE IF EXISTS employees1;
DROP TABLE IF EXISTS employees2;
DROP TABLE IF EXISTS employees3;

--  CREATE TABLE employees1 (
    --  emp_no     INTEGER     PRIMARY KEY,
    --  birth_date DATE        NOT NULL,
    --  first_name VARCHAR(14) NOT NULL,
    --  last_name  VARCHAR(16) NOT NULL,
    --  gender     STRING      NOT NULL,
    --  hire_date  DATE        NOT NULL
--  );
--
--  CREATE TABLE employees2 (
    --  emp_no     INTEGER     PRIMARY KEY,
    --  birth_date DATE        NOT NULL,
    --  first_name VARCHAR(14) NOT NULL,
    --  last_name  VARCHAR(16) NOT NULL,
    --  gender     STRING      NOT NULL,
    --  hire_date  DATE        NOT NULL
--  );
--
--  CREATE TABLE employees3 (
    --  emp_no     INTEGER     PRIMARY KEY,
    --  birth_date DATE        NOT NULL,
    --  first_name VARCHAR(14) NOT NULL,
    --  last_name  VARCHAR(16) NOT NULL,
    --  gender     STRING      NOT NULL,
    --  hire_date  DATE        NOT NULL
--  );
--

IMPORT TABLE employees1 (
   	emp_no     INTEGER     PRIMARY KEY,
	birth_date DATE        NOT NULL,
	first_name VARCHAR(14) NOT NULL,
	last_name  VARCHAR(16) NOT NULL,
	gender     STRING      NOT NULL,
	hire_date  DATE        NOT NULL
  ) CSV DATA('nodelocal:///employees1.csv');
IMPORT TABLE employees2 (
   	emp_no     INTEGER     PRIMARY KEY,
	birth_date DATE        NOT NULL,
	first_name VARCHAR(14) NOT NULL,
	last_name  VARCHAR(16) NOT NULL,
	gender     STRING      NOT NULL,
	hire_date  DATE        NOT NULL
  ) CSV DATA('nodelocal:///employees2.csv');
IMPORT TABLE employees3 (
   	emp_no     INTEGER     PRIMARY KEY,
	birth_date DATE        NOT NULL,
	first_name VARCHAR(14) NOT NULL,
	last_name  VARCHAR(16) NOT NULL,
	gender     STRING      NOT NULL,
	hire_date  DATE        NOT NULL
  ) CSV DATA('nodelocal:///employees3.csv');

GRANT ALL ON TABLE ddbms_cockroach.* TO ddbms;
