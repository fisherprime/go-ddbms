-- Cannot use password authentication in insecure mode
CREATE USER ddbms;
CREATE DATABASE IF NOT EXISTS ddbms_cockroach;

GRANT ALL ON DATABASE ddbms_cockroach TO ddbms;

show databases;
