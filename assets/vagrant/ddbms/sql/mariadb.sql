USE ddbms_mariadb;

DROP TABLE IF EXISTS dept_manager2;
DROP TABLE IF EXISTS salaries1;
DROP TABLE IF EXISTS salaries2;
DROP TABLE IF EXISTS salaries3;
DROP TABLE IF EXISTS salaries4;
DROP TABLE IF EXISTS salaries5;

CREATE TABLE dept_manager2 (
   emp_no    INTEGER PRIMARY KEY,
   from_date DATE    NOT NULL,
   to_date   DATE    NOT NULL
);

CREATE TABLE salaries1 (
    emp_no    INT  NOT NULL,
    salary    INT  NOT NULL,
    from_date DATE NOT NULL,
    to_date   DATE NOT NULL
);

CREATE TABLE salaries2 (
    emp_no    INT  NOT NULL,
    salary    INT  NOT NULL,
    from_date DATE NOT NULL,
    to_date   DATE NOT NULL
);

CREATE TABLE salaries3 (
    emp_no    INT  NOT NULL,
    salary    INT  NOT NULL,
    from_date DATE NOT NULL,
    to_date   DATE NOT NULL
);

CREATE TABLE salaries4 (
    emp_no    INT  NOT NULL,
    salary    INT  NOT NULL,
    from_date DATE NOT NULL,
    to_date   DATE NOT NULL
);

CREATE TABLE salaries5 (
    emp_no    INT  NOT NULL,
    salary    INT  NOT NULL,
    from_date DATE NOT NULL,
    to_date   DATE NOT NULL
);

SELECT 'LOADING dept_manager2' as 'INFO';
LOAD DATA INFILE '/vagrant_data/data/dept_manager2.csv' INTO TABLE dept_manager2 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';

SELECT 'LOADING salaries1' as 'INFO';
LOAD DATA INFILE '/vagrant_data/data/salaries1.csv' INTO TABLE salaries1 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';
SELECT 'LOADING salaries2' as 'INFO';
LOAD DATA INFILE '/vagrant_data/data/salaries2.csv' INTO TABLE salaries2 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';
SELECT 'LOADING salaries3' as 'INFO';
LOAD DATA INFILE '/vagrant_data/data/salaries3.csv' INTO TABLE salaries3 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';
SELECT 'LOADING salaries4' as 'INFO';
LOAD DATA INFILE '/vagrant_data/data/salaries4.csv' INTO TABLE salaries4 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';
SELECT 'LOADING salaries5' as 'INFO';
LOAD DATA INFILE '/vagrant_data/data/salaries5.csv' INTO TABLE salaries5 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';
