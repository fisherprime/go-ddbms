b="*******************"
cockroach_bin="/usr/local/bin/cockroach"

printf 'Installing net-tools\n'
# sudo yum check-update
sudo yum -y install net-tools

printf "%s Setting root password %s" "$b" "$b"
echo -e 'vagrant\nvagrant' | sudo passwd root

if [[ ! -x $cockroach_bin ]]; then
	printf "%s Sourcing CockroachDB %s\n" "$b" "$b"
	cd /vagrant_data || exit

	if [[ ! -f cockroach-latest.linux-amd64.tgz ]]; then
		wget --quiet https://binaries.cockroachdb.com/cockroach-latest.linux-amd64.tgz
	fi

	mkdir -p out
	tar --no-same-owner -xvf cockroach-latest.linux-amd64.tgz
	sudo mv "$(echo cockroach-v*.linux-amd64/cockroach)" /usr/local/bin
	rm -r out
fi

cd /home/vagrant || exit

sudo bash <<CMD
printf 'session    required   pam_limits.so\n' >>/etc/pam.d/common-session
printf 'session    required   pam_limits.so\n' >>/etc/pam.d/common-session-noninteractive
cat >>/etc/security/limits.conf <<EOF
*              soft     nofile          35000
*              hard     nofile          35000
root           soft     nofile          35000
root           hard     nofile          35000
EOF
CMD

printf "%s Initializing CockroachDB cluster %s\n" "$b" "$b"
[[ $(pgrep -c cockroach) -gt 0 ]] && $cockroach_bin quit --insecure
[[ -d cockroach-data ]] && rm -rf cockroach-data/

mkdir -p cockroach-data/extern
cp /vagrant_data/data/employees* cockroach-data/extern
$cockroach_bin start --insecure --listen-addr=0.0.0.0 --http-addr=0.0.0.0 --background
sleep 2

printf "%s Creating ddbms user and database(CockroachDB) %s\n" "$b" "$b"
$cockroach_bin sql --insecure --host=localhost:26257 --echo-sql </vagrant_data/sql/cockroach_setup.sql

printf "%s Populating ddbms_cockroach %s\n" "$b" "$b"
$cockroach_bin sql --insecure --host=localhost:26257 --echo-sql -d ddbms_cockroach </vagrant_data/sql/cockroach.sql

wait
printf "%s CockroachDB setup complete %s\n" "$b" "$b"
