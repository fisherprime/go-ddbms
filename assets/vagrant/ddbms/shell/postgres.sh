b="*******************"

printf "%s Installing PostgreSQL %s\n" "$b" "$b"
apt-get install -y net-tools postgresql-9.6

printf "%s Configuring and restarting PostgreSQL %s\n" "$b" "$b"
bash <<CMD
printf 'listen_addresses = '"'"'*'"'\n" >>"/etc/postgresql/9.6/main/postgresql.conf"
cat >>"/etc/postgresql/9.6/main/pg_hba.conf" <<EOF
host    all             all             0.0.0.0/0            md5
local   all             all                                  md5
EOF
systemctl restart postgresql.service
CMD

printf "%s Creating ddbms user and database(PostgreSQL) %s\n" "$b" "$b"
sudo -u postgres psql -a -f /vagrant_data/sql/postgres_setup.sql

printf "%s Populating database(ddbms_postgres) %s\n" "$b" "$b"
PGPASSWORD='ddbms' sudo -u postgres psql ddbms_postgres -f /vagrant_data/sql/postgres.sql

printf "%s PostgreSQL setup complete %s\n" "$b" "$b"
