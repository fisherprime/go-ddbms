b="*******************"

printf "%s Installing MariaDB %s\n" "$b" "$b"
apt-get install -y net-tools mariadb-client mariadb-server

printf "%s Configuring & restarting MariaDB service %s\n" "$b" "$b"
cp /vagrant_data/shell/my.cnf /etc/mysql
systemctl restart mariadb.service

printf "%s Creating ddbms user & database(MariaDB) %s\n" "$b" "$b"
mysql </vagrant_data/sql/mariadb_setup.sql

printf "%s Populating database(ddbms_mariadb) %s\n" "$b" "$b"
mysql </vagrant_data/sql/mariadb.sql

printf "%s MariaDB setup complete %s\n" "$b" "$b"
