// Package main provides the go-ddbms middleware binary.
package main

import (
	"flag"
	"fmt"
	"os"

	ddbms "gitlab.com/fisherprime/go-ddbms/internal/ddbms"

	log "github.com/sirupsen/logrus"
)

var (
	Build, Version string
)

func main() {
	const project = "Go DDBMS"

	var (
		args struct {
			PrintVersion bool
		}
	)

	flags := flag.NewFlagSet(project, flag.ExitOnError)
	flags.BoolVar(&args.PrintVersion, "V", false, "Print version information")

	if err := flags.Parse(os.Args[1:]); err != nil {
		log.Fatal(err)
	}

	switch {
	case args.PrintVersion:
		fmt.Printf("%s %s \nBuild: %s\n", project, Version, Build)
		return
	default:
		if err := ddbms.StartServer(); err != nil {
			log.Fatal(err)
		}

	}

	log.Exit(0)
}
