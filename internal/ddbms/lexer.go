package ddbms

import (
	"context"
	"errors"
	"fmt"
	"io"
	"strings"
	"unicode"
	"unicode/utf8"
)

// REF: https://github.com/sh4t/sql-parser
// A lexer that runs at its own pace separate from the parser

type (
	// ItemID int holding an identifier for an Item's token.
	ItemID int

	// NextOperation type for the next function to be executed.
	NextOperation func() NextOperation

	// ValidationFunction type for functions that validate rune identities.
	ValidationFunction func(rune) bool

	// Item type containing a lexed token.
	Item struct {
		// The type of this Item.
		id ItemID
		// The starting position (in bytes) of the contained token; referencing the source.
		pos int
		// The lexed token.
		tok string
		// Encountered error during the lexing process.
		err error
	}

	// Lexer type to capture identifiers from a string.
	Lexer struct {
		ctx context.Context

		// source is the input string converted to a `RuneReader`.
		source io.RuneReader
		// sourceIndex is the start position of the current `Item`'s rune in the source.
		sourceIndex int

		// buffer is a slice of runes read from the source for lexing.
		buffer []rune
		// current buffer position.
		bufferIndex int

		// channel for communicating scanned `item`s.
		item chan Item
	}
)

// iota is used to define an incrementing number sequence for const
// declarations
const (
	_                     = iota // Consume 0 to start actual numbering at 1
	ItemComma                    // ','
	ItemDot                      // '.'
	ItemEOF                      // End of the file
	ItemError                    // Error occurred
	ItemIdentifier               // Alphanumeric or complex identifier like `a.b` and `c`.*
	ItemKeyword                  // SQL keyword
	ItemLeftParen                // '('
	ItemMultiLineComment         // A multiline comment like /* ... */
	ItemNumber                   // Real number
	ItemOperator                 // Operators like '=', '<>', etc
	ItemRightParen               // ')'
	ItemSemi                     // ';'
	ItemSingleLineComment        // A comment like --
	ItemString                   // Quoted string (includes quotes)
	ItemWhitespace               // Space, tab / newline

	backSlash   = '\\'
	backTick    = '`'
	comma       = ','
	dot         = '.'
	doubleQuote = '"'
	leftParen   = '('
	rightParen  = ')'
	semiColon   = ';'
	singleQuote = '\''

	// Keywords.
	andK      = "and"
	asK       = "as"
	descK     = "desc"
	distinctK = "distinct"
	fromK     = "from"
	groupK    = "group"
	havingK   = "having"
	insertK   = "insert"
	intoK     = "into"
	joinK     = "join"
	limitK    = "limit"
	lockingK  = "locking"
	notK      = "not"
	orK       = "or"
	orderByK  = "order by"
	selectK   = "select"
	setK      = "set"
	sortK     = "sort"
	unionK    = "union"
	updateK   = "update"
	whereK    = "where"
	windowK   = "window"
	withK     = "with"

	// Using max packet size as limit.
	queryLimit = 65535

	lexBufferSize = 10
)

var (
	itemNames = map[ItemID]string{
		ItemComma:      "comma",
		ItemEOF:        "EOF",
		ItemError:      "error",
		ItemIdentifier: "identifier",
		ItemKeyword:    "keyword",
		ItemLeftParen:  "left_paren",
		ItemNumber:     "number",
		ItemOperator:   "operator",
		ItemRightParen: "right_paren",
		ItemSemi:       "stmt_end",
		ItemString:     "string",
		ItemWhitespace: "whitespace",
	}

	sqlKeywords = []string{
		andK, asK, fromK, insertK, intoK, joinK, notK, orK, selectK, setK, unionK, updateK, whereK, orderByK,
	}

	errFieldNotComplete = errors.New("field not fully referenced")
	errUnterminatedStr  = errors.New("quoted string not terminated")
)

// newLexer creates a new `Lexer` with a scanner (for the input source), buffer & channel for the
// `Item` communication.
func newLexer(ctx context.Context, source string) *Lexer {
	logger.Info("Init query lexer")

	return &Lexer{
		ctx:    ctx,
		buffer: make([]rune, lexBufferSize),
		item:   make(chan Item),
		source: strings.NewReader(source),
	}
}

// Lex the input by executing state functions.
func (l *Lexer) Lex() {
	for stateFunction := l.LexWhitespace; stateFunction != nil; {
		stateFunction = stateFunction()
	}

	// Close channel.
	close(l.item)
}

// LexWhitespace search for whitespace.
func (l *Lexer) LexWhitespace() NextOperation {
	select {
	case <-l.ctx.Done():
		return nil
	default:
		if err := l.AcceptWhile(isWhitespace); err != nil {
			return l.LogError(err)
		}
		if l.bufferIndex > 0 {
			l.Emit(ItemWhitespace)
		}

		next, err := l.Peek()
		if err != nil {
			return l.LogError(err)
		}

		switch {
		case next == leftParen:
			l.Advance()
			l.Emit(ItemLeftParen)

			return l.LexWhitespace()
		case next == rightParen:
			l.Advance()
			l.Emit(ItemRightParen)

			return l.LexWhitespace()
		case next == comma:
			l.Advance()
			l.Emit(ItemComma)

			return l.LexWhitespace()
		case next == semiColon:
			l.Advance()
			l.Emit(ItemSemi)

			return l.LexWhitespace()
		case isOperator(next):
			return l.LexOperator()
		case next == doubleQuote || next == singleQuote:
			return l.LexString()
		case isNumeric(next):
			return l.LexNumber()
		case isAlphaNumeric(next) || next == backTick:
			return l.LexIdentifierOrKeyword()
		default:
			nextRunes, err := l.PeekNext(queryLimit)
			if err != nil {
				return l.LogError(err)
			}
			l.LogError(fmt.Errorf("unknown operation for: %v", string(nextRunes)))

			return nil
		}
	}
}

// LexOperator search for arithmetic operators.
func (l *Lexer) LexOperator() NextOperation {
	select {
	case <-l.ctx.Done():
		return nil
	default:
		l.AcceptWhile(isOperator)
		l.Emit(ItemOperator)

		return l.LexWhitespace()
	}
}

// LexNumber search for real numbers.
func (l *Lexer) LexNumber() NextOperation {
	var err error

	select {
	case <-l.ctx.Done():
		return nil
	default:
		initialIndex := l.bufferIndex
		if err = l.AcceptWhile(unicode.IsDigit); err != nil {
			break
		}

		// Accept decimals
		if l.Accept(".") {
			l.AcceptWhile(unicode.IsDigit)
		}

		// Accept exponents.
		if l.Accept("eE") {
			l.Accept("+-")
			if err = l.AcceptWhile(unicode.IsDigit); err != nil {
				break
			}
		}

		// If the item is an identifier (not a number), revert the index to before the object
		var next rune
		if next, err = l.Peek(); err != nil {
			break
		}
		if isAlphaNumeric(next) {
			if err = l.BackupFor(initialIndex - l.bufferIndex); err != nil {
				break
			}
			return l.LexIdentifierOrKeyword()
		}

		// Emit lexed number.
		l.Emit(ItemNumber)

		return l.LexWhitespace()
	}

	return l.LogError(err)
}

// LexString search for string.
func (l *Lexer) LexString() NextOperation {
	var err error

	select {
	case <-l.ctx.Done():
		return nil
	default:
		var quote rune
		if quote, err = l.Next(); err != nil {
			return l.LogError(err)
		}

		for {
			var next rune
			if next, err = l.Next(); err != nil {
				// Won't play nice with NO_BACKSLASH_ESCAPES
				break
			}

			switch next {
			case backSlash:
				if _, err := l.Peek(); err != nil {
					break
				}
				l.Advance()
			case quote:
				// End of string.
				var r rune
				if r, err = l.Peek(); err != nil {
					break
				}
				if r == quote {
					l.Advance()
					continue
				}

				l.Emit(ItemString)

				return l.LexWhitespace()
			default:
				// String characters have been consumed.
			}

			if err != nil {
				break
			}
		}

		if err == io.EOF {
			err = errUnterminatedStr
		}
		return l.LogError(err)
	}
}

// LexIdentifierOrKeyword search for identifiers / SQL keywords
func (l *Lexer) LexIdentifierOrKeyword() NextOperation {
	var err error

	select {
	case <-l.ctx.Done():
		return nil
	default:
		for {
			var char rune
			if char, err = l.Next(); err != nil {
				break
			}

			switch {
			case char == backTick:
				// Quoted identifier using backticks.
				for {
					var r rune
					if r, err = l.Next(); err != nil {
						break
					}

					if r == backTick {
						if r, err = l.Peek(); err != nil {
							break
						}
						if r != backTick {
							// Continue for loop execution.
							break
						}
						// Consume the backtick.
						l.Advance()
					}
				}
				if err != nil {
					break
				}

				l.Emit(ItemIdentifier)
			case isAlphaNumeric(char):
				var isKeyword bool

				if err = l.AcceptWhile(isAlphaNumeric); err != nil {
					break
				}

				// Keyword check
				for _, str := range sqlKeywords {
					if strings.EqualFold(str, string(l.buffer[:l.bufferIndex])) {
						l.Emit(ItemKeyword)
						isKeyword = true

						break
					}
				}

				if !isKeyword {
					var r rune
					if r, err = l.Peek(); err != nil {
						break
					}
					if r == dot {
						l.Advance()

						if r, err = l.Peek(); err != nil {
							break
						}
						if !isAlphaNumeric(r) {
							return l.LogError(errFieldNotComplete)
						}

						l.AcceptWhile(isAlphaNumeric)
					}

					l.Emit(ItemIdentifier)
				}
			}

			l.AcceptWhile(isWhitespace)
			if l.bufferIndex > 0 {
				l.Emit(ItemWhitespace)
			} else {
				break
			}
		}

		if err == nil {
			return l.LexWhitespace()
		}

		if err == io.EOF {
			err = errUnterminatedStr
		}
		return l.LogError(err)
	}
}

// Advance advances the buffer & it's index suppressing any error.
func (l *Lexer) Advance() { _, _ = l.Next() }

// Next return the Next rune in the input.
func (l *Lexer) Next() (r rune, err error) {
	if l.bufferIndex < len(l.buffer) {
		// Refer to `Lexer.Backup` for the purpose of this.
		l.bufferIndex++
		r = l.buffer[l.bufferIndex-1]

		return
	}

	// Read from scanner.
	r, _, err = l.source.ReadRune()
	if err != nil {
		return
	}

	// Append rune to the buffer & update the buffer index.
	l.buffer = append(l.buffer, r)
	l.bufferIndex++

	return
}

// Peek return next rune, don't update index
func (l *Lexer) Peek() (r rune, err error) {
	if l.bufferIndex < len(l.buffer) {
		r = l.buffer[l.bufferIndex]
		return
	}

	if _, err = l.PeekNext(1); err != nil {
		r = l.buffer[l.bufferIndex]
	}

	return
}

// PeekNext return next N runes, without updating the index
func (l *Lexer) PeekNext(length int) (rList []rune, err error) {
	if length < 1 {
		err = fmt.Errorf("invalid peek length: %d", length)
		return
	}

	if l.bufferIndex+length < len(l.buffer) {
		rList = l.buffer[l.bufferIndex:(l.bufferIndex + length)]
		return
	}

	var seen = 0
	for ; seen < length; seen++ {
		var r rune

		if r, _, err = l.source.ReadRune(); err != nil {
			if err != io.EOF {
				return
			}
			err = nil

			break
		}

		l.buffer = append(l.buffer, r)
	}
	rList = l.buffer[l.bufferIndex:(l.bufferIndex + length)]

	return
}

// Backup step back one rune
func (l *Lexer) Backup() (err error) { return l.BackupFor(1) }

// BackupFor step back for N runes.
func (l *Lexer) BackupFor(length int) (err error) {
	if l.bufferIndex < length {
		err = fmt.Errorf("attempt to back up (%d) spaces on buffer index: %d\n", length,
			l.bufferIndex)
		return
	}
	l.bufferIndex -= length

	return
}

// Emit send an Item specification to the LexType
func (l *Lexer) Emit(t ItemID) {
	l.item <- Item{
		id:  t,
		pos: l.sourceIndex,
		tok: string(l.buffer[:l.bufferIndex]),
	}
	l.Ignore()
}

// Ignore skip scanner input before the current buffer index
func (l *Lexer) Ignore() {
	itemBytes := 0
	for i := 0; i < l.bufferIndex; i++ {
		itemBytes += utf8.RuneLen(l.buffer[i])
	}

	l.sourceIndex += itemBytes
	l.buffer = l.buffer[l.bufferIndex:]

	l.bufferIndex = 0
}

// Accept consumes the next rune if it's from the valid set
func (l *Lexer) Accept(valid string) bool {
	r, err := l.Next()
	if err != nil {
		return false
	}

	// Return first instance of Unicode codepoint defined by the rune in the string
	if strings.IndexRune(valid, r) >= 0 {
		return true
	}

	_ = l.Backup()

	return false
}

// AcceptWhile consumes runes while condition is true.
func (l *Lexer) AcceptWhile(fn ValidationFunction) (err error) {
	r, err := l.Next()
	if err != nil {
		return
	}

	for fn(r) {
		if r, err = l.Next(); err != nil {
			return
		}
	}

	err = l.Backup()

	return
}

// AcceptUntil consumes runes until the condition is met.
func (l *Lexer) AcceptUntil(fn ValidationFunction) (err error) {
	r, err := l.Next()
	if err != nil {
		return
	}

	for !fn(r) {
		if r, err = l.Next(); err != nil {
			return
		}
	}

	err = l.Backup()

	return
}

// LogError set the item to the error token that'll terminate the scan process
// (nextItem)
func (l *Lexer) LogError(err error) NextOperation {
	if err == io.EOF {
		l.Emit(ItemEOF)
		return nil
	}

	l.item <- Item{
		id:  ItemError,
		pos: l.sourceIndex,
		err: err,
	}
	return nil
}

// NextItem return the next Item from the input
func (l *Lexer) NextItem() Item { return <-l.item }

// isSpace return true for whitespace, newline & carrier return
func isWhitespace(r rune) bool { return isSpace(r) || r == '\r' || r == '\n' }

// isSpace return true for space or tab
func isSpace(r rune) bool { return r == ' ' || r == '\t' }

// isEndOfLine return true for end-of-line
func isEndOfLine(r rune) bool { return r == '\r' || r == '\n' }

// isAlpha return true for an alphabetic sequence
func isAlpha(r rune) bool { return r == '_' || unicode.IsLetter(r) || r == '*' }

// isNumeric return true for a real number
func isNumeric(r rune) bool { return unicode.IsDigit(r) }

// isNumeric return true for an alphanumeric sequence
func isAlphaNumeric(r rune) bool { return isAlpha(r) || isNumeric(r) }

// isOperator report math operator
func isOperator(r rune) bool {
	return r == '+' || r == '-' || r == '*' || r == '/' || r == '=' || r == '>' || r == '<' || r ==
		'~' || r == '|' || r == '^' || r == '&' || r == '%'
}
