package ddbms

import (
	"errors"
	"net/http"

	"github.com/sirupsen/logrus"
)

// REF: https://github.com/volatiletech/authboss/blob/master/defaults/error_handler.go

type (
	// ErrorHandler wraps http handlers returning an error to provide error handling.
	//
	// The pieces provided to this struct must be thread-safe since they will be
	// handed to many
	// pointers to themselves.
	ErrorHandler struct {
		LogWriter *logrus.Logger
	}

	errHandler struct {
		// Handler is a request handler returning an error.
		Handler   func(wr http.ResponseWriter, req *http.Request) error
		LogWriter *logrus.Logger
	}
)

var (
	// ErrForbidden is returned on an unprivileged operation.
	ErrForbidden = errors.New(http.StatusText(http.StatusForbidden))

	statusCodeCtxKey = "status code"
)

// newErrorHandler constructor.
func newErrorHandler(logger *logrus.Logger) *ErrorHandler {
	return &ErrorHandler{LogWriter: logger}
}

// ChiWrap a request handler that returns an error.
func (e ErrorHandler) Wrap(handler func(wr http.ResponseWriter, req *http.Request) error) http.HandlerFunc {
	return errHandler{Handler: handler, LogWriter: e.LogWriter}.ServeHTTP
}

// ServeHTTP handles errors.
func (e errHandler) ServeHTTP(wr http.ResponseWriter, req *http.Request) {
	err := e.Handler(wr, req)
	if sessionCookie := wr.Header().Get("Set-Cookie"); sessionCookie != "" {
		wr.Header().Add("Session-Token", sessionCookie)
	}
	if err == nil {
		return
	}

	var (
		code int
		ok   bool
	)

	if code, ok = req.Context().Value(statusCodeCtxKey).(int); ok {
		if code < http.StatusBadRequest || code > http.StatusNetworkAuthenticationRequired {
			// Ignore the status in the context.
			code = http.StatusInternalServerError
		}
	} else {
		// Handle unset context.
		code = http.StatusInternalServerError
	}

	responder.Respond(wr, req, code, HTMLData{"error": err})
}
