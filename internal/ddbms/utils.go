package ddbms

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"time"
)

// fileDeleteDelay variable holding the system's delay time before deleting
// a file
var (
	fileDeleteDelay = 120 * time.Second
)

// GetFileContentType retreives the content-type of a file from it's first 512
// bytes
func GetFileContentType(out *os.File) (string, error) {
	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err := out.Read(buffer)
	if CheckError("[!] Couldn't get file content-type, ", err) {
		return "", err
	}

	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}

// PendFileDelete create a waiting job to delete a file
func PendFileDelete(filePath string) {
	// Check for existing file
	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		logger.Errorf("%s doesn't exist (%v)", filePath, err)

		return
	}

	time.Sleep(fileDeleteDelay)
	os.Remove(filePath)

	logger.Info("Deleted file: ", filePath)
}

// CheckError checks the error variable, if set prints out the error
// message appended to a user chosen string then returns true, else
// returns false
func CheckError(message string, err error) bool {
	if err == nil {
		return false
	}

	if message == "" {
		logger.Error(err)
		return true
	}
	logger.WithError(err).Error(message)

	return true
}

// CheckErrorFatal checks the error variable, if set prints out the error
// message appended to a user chosen string then returns true, else
// returns false
func CheckErrorFatal(message string, err error) {
	if err == nil {
		return
	}

	if message == "" {
		logger.Fatal(err)
	}

	logger.Fatal(fmt.Sprintf("%s,", message), err)
}

// CreateFile creates a file if it doesn't exist
func CreateFile(filePath string) {
	fileDir := filepath.Dir(filePath)

	_, err := os.Stat(fileDir)
	if os.IsNotExist(err) {
		if err := os.MkdirAll(fileDir, 0644); CheckError(fmt.Sprintf("[!] Error occured while creating directory hierarchy: %s", fileDir), err) {
			return
		}
	}

	_, err = os.Stat(filePath)

	if os.IsNotExist(err) {
		file, err := os.Create(filePath)
		if CheckError(fmt.Sprintf("[!] Error occurred while creating file: %s", filePath), err) {
			return
		}

		defer file.Close()
	}

	logger.Info("Created file: ", filePath)
}

// WaitUntilFileExists waits for a file to exist before exiting with a nil
// status, an error should 2 minutes pass before the file is available
func WaitUntilFileExists(filePath string) error {
	stop := time.Now().Add(120 * time.Second)

	for {
		_, err := os.Stat(filePath)

		if os.IsNotExist(err) {
			if time.Now().After(stop) {
				return err
			}

			time.Sleep(1 * time.Second)

			continue
		}

		return nil
	}
}

// sliceContains searches for the occurence of a value in an untyped slice
func sliceContains(container interface{}, value interface{}) bool {
	typedSlice := reflect.ValueOf(container)

	if typedSlice.Kind() == reflect.Slice {
		for i := 0; i < typedSlice.Len(); i++ {
			// FIXME: panics if slice element points to an unexported struct field
			// REFACTOR SEE https://golang.org/pkg/reflect/#Value.Interface
			if typedSlice.Index(i).Interface() == value {
				return true
			}
		}
	}

	return false
}

// GeneratePermutations generates all possible permutations for some input slice
//
// Using Heap's algorithm.
//
// TODO: Test.
func GeneratePermutations(in interface{}) interface{} {
	var permute func([]interface{}, int)

	arr := in.([]interface{})
	res := [][]interface{}{}

	permute = func(arr []interface{}, n int) {
		if n == 1 {
			res = append(res, arr)
			return
		}

		for i := 0; i < n; i++ {
			permute(arr, n-1)
			if n%2 == 1 {
				arr[i], arr[n-1] = arr[n-1], arr[i]
			} else {
				arr[0], arr[n-1] = arr[n-1], arr[0]
			}
		}
	}

	permute(arr, len(arr))

	return res
}
