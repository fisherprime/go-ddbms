package ddbms

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
)

type (
	SiteDetails struct {
		conn *sql.DB
		tx   *sql.Tx

		query             string
		fKeys, name, pKey string
		size              int
	}

	SiteMergeDetails struct {
		dets         *SiteDetails
		pKeys, fKeys string
	}

	CandidateCosts struct {
		candidates [][]*SiteDetails
		costs      []int
	}
)

var (
	errInvalidSchema      = errors.New("the query's referenced schema(s) do not exist")
	errGenFragmentQueries = errors.New("failed to generate fragment query candidates")
)

// Optimize optimizes the fragment queries.
func (q *Query) Optimize() (optimalOrder []*SiteDetails, err error) {
	var (
		cCosts       CandidateCosts
		initialOrder []*SiteDetails
	)

	logger.Info("Init query optimizer")

	for _, query := range q.fragmentQueries {
		for _, db := range dbDetailsMap {
			for _, fragment := range db.fragments {
				if strings.Contains(query, fragment.name) {
					initialOrder = append(initialOrder, &SiteDetails{
						conn:  db.conn,
						fKeys: fragment.fKeys,
						name:  fragment.name,
						pKey:  fragment.pKey,
						size:  fragment.size,
					})
					logger.Debugf("Fragment(%d): {name=%s, size=%d}\n", len(initialOrder),
						fragment.name, fragment.size)

					break
				}
			}
		}
	}
	if len(initialOrder) < 1 {
		err = errInvalidSchema
		return
	}

	// Generate possible fragment queries.
	if cCosts.candidates = generateFragmentQueries(initialOrder); cCosts.candidates == nil {
		err = errGenFragmentQueries
		return
	}
	// Populate fragment query costs.
	for _, candidate := range cCosts.candidates {
		cCosts.costs = append(cCosts.costs, calculateCandidateCost(&candidate))
	}

	if Debug {
		logCandidates(&cCosts)
	}

	optimalOrder = cCosts.candidates[0]
	optimalCost := cCosts.costs[0]
	for index, cost := range cCosts.costs[1:] {
		logger.Debugf("Fragment(%d): {split cost=%d, optimal split cost=%d}\n", index+1, cost,
			optimalCost)

		if cost < optimalCost {
			optimalCost = cost
			// index starts counting at the second array element.
			optimalOrder = cCosts.candidates[index+1]
		}
	}

	return
}

// calculateCandidateCost calculate the cost for using a fragment query.
//
// TODO: Review calculation logic.
func calculateCandidateCost(candidate *[]*SiteDetails) (cost int) {
	var prevNode *SiteMergeDetails

	for _, node := range *candidate {
		var isChild bool

		if prevNode == nil {
			cost = (*node).size
			prevNode = &SiteMergeDetails{
				dets:  node,
				fKeys: node.fKeys,
				pKeys: node.pKey,
			}

			continue
		}

		if (*prevNode).pKeys != "" {
			prevPrimKeys := strings.Split((*prevNode).pKeys, ",")

			for _, pKey := range prevPrimKeys {
				// Current node is a child to the previous one.
				if strings.Contains((*node).fKeys, pKey) {
					logger.Debug((*node).name, "child")

					cost += (*node).size

					prevNode.dets = node
					prevNode.fKeys += "," + node.fKeys
					prevNode.pKeys += "," + node.pKey

					isChild = true

					break
				}
			}
			if isChild {
				continue
			}
		}

		// Current & previous nodes are related horizontal fragments.
		if (*prevNode).dets.pKey == (*node).pKey {
			cost += (*node).size

			prevNode.dets = node
			prevNode.fKeys += "," + node.fKeys
			prevNode.pKeys += "," + node.pKey

			continue
		}

		// Current node is a parent to the previous one.
		if strings.Contains((*prevNode).fKeys, (*node).pKey) {
			logger.Debug((*node).name, "parent")

			cost += (*prevNode).dets.size

			prevNode.dets = node
			prevNode.fKeys += "," + node.fKeys
			prevNode.pKeys += "," + node.pKey

			continue
		}

		cost += (*node).size * (*prevNode).dets.size

		prevNode.dets = node
		prevNode.fKeys += "," + node.fKeys
		prevNode.pKeys += "," + node.pKey
	}

	return
}

func generateFragmentQueries(arr []*SiteDetails) [][]*SiteDetails {
	if val, ok := GeneratePermutations(arr).([][]*SiteDetails); ok {
		return val
	}
	return nil
}

// logCandidates prints a "DEBUG" logger entry of the generated fragment queries.
func logCandidates(c *CandidateCosts) {
	s := strings.Builder{}

	for i, candidate := range c.candidates {
		splitLim := len(candidate) - 1
		for k, node := range candidate {
			fmt.Fprintf(&s, "%s(%d)", node.name, node.size)
			if k < splitLim {
				_, _ = fmt.Fprintf(&s, "->")
			}
		}
		logger.Debug("Candidate(%d): [%s], cost: %d\n", i, s.String(), c.costs[i])
		s.Reset()
	}
}
