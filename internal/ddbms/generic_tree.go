package ddbms

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

// NOTE: This gets messy when a role is spread across multiple levels: clerk.

// REF: https://www.geeksforgeeks.org/generic-tree-level-order-traversal
//
// REF: https://www.geeksforgeeks.org/serialize-deserialize-n-ary-tree
//
// `rune`s are singe `UTF-8` codepoints, not used as the AST expects strings.

type (
	// TreeNode defines an n-array tree to hold an AST / parse tree.
	TreeNode struct {
		ctx context.Context

		data string

		// operator holds the node at the upper level, operation to perform.
		operator *TreeNode

		// operands holds nodes at a lower level, operands to operation.
		operands []*TreeNode
	}

	// TreeTraverseChan defines a channel to communicate info between `TreeNode` operations & it's
	// callers.
	TreeTraverseChan struct {
		newOperation bool
		operand      *TreeNode
		err          error
	}
)

const (
	initalQueueLen = 1

	// valueSplitter is the character used to split the `TreeNode` serialization output.
	valueSplitter = " "

	traverseBufferSize = 10
)

var (
	// Errors codes encountered when handling a `TreeNode`.
	errAlreadySub   = errors.New("is already an operand of")
	errIDNotFound   = errors.New("not found")
	errIDNotOperand = errors.New("is not an operand of")
	errNoOperands   = errors.New("lacks operands")
)

// newTree initiates a `TreeNode`.
func newTree(ctx context.Context, init string) *TreeNode {
	return &TreeNode{
		ctx:      ctx,
		data:     init,
		operands: []*TreeNode{},
	}
}

// AddOperatorOperand to a `TreeNode`.
func (h *TreeNode) AddOperatorOperand(operator, operand string) (err error) {
	var operatorNode *TreeNode
	if operatorNode = h.LocateID(operator); operatorNode == nil {
		err = fmt.Errorf("operator (%s) %w", operator, errIDNotFound)
		return
	}

	// Search for existing immediate operand.
	for _, sub := range operatorNode.operands {
		if sub.data == operand {
			err = fmt.Errorf("(%s) %w (%s)", operand, errAlreadySub, operator)
			return
		}
	}
	operatorNode.operands = append(operatorNode.operands, newTree(h.ctx, operand))

	return
}

// AddOperand to a `TreeNode`.
func (h *TreeNode) AddOperand(operand string) (err error) {
	// Search for existing immediate operand.
	for _, sub := range h.operands {
		if sub.data == operand {
			err = fmt.Errorf("(%s) %w (%s)", operand, errAlreadySub, h.data)
			return
		}
	}
	h.operands = append(h.operands, newTree(h.ctx, operand))

	return
}

// ModifyNode alters the `data` stored in a `TreeNode`.
func (h *TreeNode) ModifyNode(id, val string) (err error) {
	if node := h.LocateID(id); node != nil {
		node.data = val
		return
	}
	err = fmt.Errorf("(%s) %w", id, errIDNotFound)

	return
}

// Walk performs level-order traversal on a `TreeNode`, pushing its values to its channel
// argument.
//
// This operation uses channels to minimize resource wastage.
// A `context.Context` is used to terminate the walk operation.
func (h *TreeNode) Walk(astChan chan TreeTraverseChan) {
	defer close(astChan)
	if h == nil {
		return
	}

	// Level order traversal.
	queue := make([]*TreeNode, initalQueueLen)
	queue = append(queue, h)

	for {
		qLen := len(queue)
		if qLen < 1 {
			break
		}

		// Iterate over the node's children.
		newBranch := true
		for {
			var terminate bool

			select {
			case <-h.ctx.Done():
				// Received context cancelation.
				return
			default:
				// Default operation is to walk.
				if qLen < 1 {
					terminate = true
					break
				}

				// Pop from queue.
				var front *TreeNode
				front, queue = queue[0], queue[1:]
				qLen--

				// Send node to caller via the channel.
				astChan <- TreeTraverseChan{operand: front, newOperation: newBranch}
				newBranch = false

				// Add children to the queue.
				if front.operands != nil {
					queue = append(queue, front.operands...)
				}
			}

			if terminate {
				break
			}
		}
	}
}

// WalkOperands traverses a `TreeNode` pushing its values to a channel argument.
//
// Using a channel allowing for processing on a returned value as more are obtained.
func (h *TreeNode) WalkOperands(operator string, astChan chan TreeTraverseChan) {
	if node := h.LocateID(operator); node != nil {
		node.Walk(astChan)
	}
}

// ListOperands returns a list of operands as defined in `TreeNode` for some operator entity.
//
// NOTE: This operation is expensive.
func (h *TreeNode) ListOperands(operator string) (subItems []string, err error) {
	astChan := make(chan TreeTraverseChan, traverseBufferSize)

	go func() { h.WalkOperands(operator, astChan) }()

	for {
		resl, proceed := <-astChan
		if !proceed || resl.operand == nil {
			break
		}
		if resl.err != nil {
			err = resl.err
			return
		}

		subItems = append(subItems, resl.operand.data)
	}

	if len(subItems) < 1 {
		err = errNoOperands
		return
	}

	return
}

// LocateID searches for an id & returns it's `TreeNode`.
func (h *TreeNode) LocateID(id string) (node *TreeNode) {
	astChan := make(chan TreeTraverseChan)

	go func() { h.Walk(astChan) }()

	for {
		resl, proceed := <-astChan
		if !proceed || resl.operand == nil {
			break
		}

		// ID found.
		if resl.operand.data == id {
			node = resl.operand
			return
		}
	}

	return
}

// LocateOperand searches for the operand of some operator & returns it's `TreeNode`.
func (h *TreeNode) LocateOperand(ctx context.Context, operator, operand string) (operandNode *TreeNode, err error) {
	var operatorNode *TreeNode

	astChan := make(chan TreeTraverseChan)

	go func() { h.Walk(astChan) }()

	for {
		resl, proceed := <-astChan
		if !proceed || resl.operand == nil {
			break
		}

		if resl.operand.data == operand {
			err = fmt.Errorf("(%s) %w (%s)", operand, errIDNotOperand, operator)
			return
		}

		if resl.operand.data == operator {
			// ID found.
			operatorNode = resl.operand
			break
		}
	}

	if operatorNode == nil {
		err = fmt.Errorf("operator (%s) %w", operator, errIDNotFound)
		return
	}

	if operandNode = operatorNode.LocateID(operand); operandNode == nil {
		err = fmt.Errorf("operand (%s) %w", operand, errIDNotFound)
		return
	}

	return
}

// LocateOperator searches for the operator to some operand & returns it's `TreeNode`.
func (h *TreeNode) LocateOperator(ctx context.Context, id string) (operatorNode *TreeNode, err error) {
	if node := h.LocateID(id); node != nil {
		operatorNode = node.operator
		return
	}

	err = fmt.Errorf("(%s) %w", id, errIDNotFound)
	return
}

// Serialize transforms a `TreeNode` into a string.
func (h *TreeNode) Serialize(ctx context.Context) (serOut string, err error) {
	var output strings.Builder

	serChan := make(chan string)

	go func() {
		h.serialize(ctx, serChan)
		close(serChan)
	}()

	// Handle root `TreeNode`.
	fVal, fProceed := <-serChan
	if !fProceed {
		return
	}
	if _, err = output.WriteString(fVal); err != nil {
		// Invalidate serialization output.
		return
	}

	for {
		val, proceed := <-serChan
		if !proceed {
			break
		}

		if _, err = output.WriteString(valueSplitter + val); err != nil {
			// Invalidate serialization output.
			return
		}
	}
	if _, err = output.WriteRune(semiColon); err != nil {
		return
	}

	err = ctx.Err()
	serOut = output.String()

	return
}

// serialize performs the serialization grunt work.
func (h *TreeNode) serialize(ctx context.Context, serChan chan string) {
	if h == nil || h.data == "" {
		return
	}

	serChan <- h.data
	for _, operand := range h.operands {
		select {
		case <-ctx.Done():
			return
		default:
			operand.serialize(ctx, serChan)
		}
	}
}
