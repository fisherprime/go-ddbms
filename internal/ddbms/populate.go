package ddbms

// PopulateSections attempts to create a PostgreSQL-like SQL parse tree.
func (q *Query) PopulateSections() (err error) {
	var (
		whereClause, valuesList, limitList, lockingList, withList []string
	)

	if q.aliases, err = q.populateList(asK); err != nil {
		return
	}

	if q.distinctClause, err = q.populateList(distinctK); err != nil {
		return
	}
	if q.intoClause, err = q.populateList(intoK); err != nil {
		return
	}

	if q.targetList, err = q.populateList(selectK); err != nil {
		return
	}
	if q.fromClause, err = q.populateList(fromK); err != nil {
		return
	}

	// NOTE: Requires additional parsing.
	if whereClause, err = q.populateList(whereK); err != nil {
		return
	}
	for _, w := range whereClause {
		// TODO: Implement grouping operation.
		logger.Debug(w)
	}

	if q.groupClause, err = q.populateVal(groupK); err != nil {
		return
	}
	if q.havingClause, err = q.populateVal(havingK); err != nil {
		return
	}

	if q.windowClause, err = q.populateVal(windowK); err != nil {
		return
	}
	// NOTE: Requires additional parsing.
	if valuesList, err = q.populateList(setK); err != nil {
		return
	}
	for _, v := range valuesList {
		// TODO: Implement grouping operation.
		logger.Debug(v)
	}
	if q.sortClause, err = q.populateVal(sortK); err != nil {
		return
	}

	if limitList, err = q.populateList(limitK); err != nil {
		return
	}
	switch {
	case len(limitList) < 1:
	case len(limitList) > 1:
		q.limitOffset = limitList[0]
		limitList = limitList[1:]

		fallthrough
	default:
		q.limitCount = limitList[0]
	}

	// NOTE: Requires additional parsing.
	if lockingList, err = q.populateList(lockingK); err != nil {
		return
	}
	for _, l := range lockingList {
		// TODO: Implement grouping operation.
		logger.Debug(l)
	}

	// NOTE: Requires additional parsing.
	if withList, err = q.populateList(withK); err != nil {
		return
	}
	for _, w := range withList {
		// TODO: Implement grouping operation.
		logger.Debug(w)
	}

	return
}

func (q *Query) populateList(keyword string) (list []string, err error) {
	if list, err = q.parseTree.ListOperands(keyword); err != nil {
		if err != errNoOperands {
			return
		}
		err = nil
	}

	return
}

func (q *Query) populateVal(keyword string) (val string, err error) {
	var list []string
	if list, err = q.parseTree.ListOperands(keyword); err != nil {
		if err != errNoOperands {
			return
		}
		err = nil

		return
	}

	val = list[0]

	return
}
