package ddbms

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/sirupsen/logrus"
)

// REF: https://github.com/go-chi/chi/blob/master/_examples/logging/main.go

type (
	// StructuredLogger defines the customized logger for the HTTP service.
	StructuredLogger struct {
		Logger *logrus.Logger
	}

	// StructuredLoggerEntry defines the standard `StructuredLogger` entry.
	StructuredLoggerEntry struct {
		Logger logrus.FieldLogger
	}

	// LogWrap wraps `logrus.Logger`.
	LogWrapper struct {
		logger *logrus.Logger
	}
)

const (
	internalOpContextKey = "internal operation"
)

// newStructuredLogger creates a `StructuredLogger`.
func newStructuredLogger(logger *logrus.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&StructuredLogger{logger})
}

// NewLogEntry creates a new log entry on start of a request.
func (l *StructuredLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	entry := &StructuredLoggerEntry{Logger: logrus.NewEntry(l.Logger)}
	logFields := logrus.Fields{}

	logFields["ts"] = time.Now().UTC().Format(time.RFC3339)

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		logFields["req_id"] = reqID
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	logFields["http_scheme"] = scheme
	logFields["http_proto"] = r.Proto
	logFields["http_method"] = r.Method

	logFields["remote_addr"] = r.RemoteAddr
	logFields["user_agent"] = r.UserAgent()

	logFields["uri"] = fmt.Sprintf("%s://%s%s", scheme, r.Host, r.RequestURI)

	entry.Logger = entry.Logger.WithFields(logFields)

	entry.Logger.Infoln("request started")

	return entry
}

// Write writes a log entry on request completion.
func (l *StructuredLoggerEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"resp_status": status, "resp_bytes_length": bytes,
		"resp_elapsed_ms": float64(elapsed.Nanoseconds()) / 1000000.0,
	})

	l.Logger.Infoln("request complete")
}

// Panic logs a panic: stack & message.
func (l *StructuredLoggerEntry) Panic(v interface{}, stack []byte) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"stack": string(stack),
		"panic": fmt.Sprintf("%+v", v),
	})
}

// Helper methods used by the application to get the request-scoped logger entry and set additional
// fields between handlers.
//
// This is a useful pattern to use to set state on the entry as it passes through the handler chain,
// which at any point can be logged with a call to .Print(), .Info(), etc.

// getLogEntry returns a `StructuredLoggerEntry`.
func getLogEntry(r *http.Request) logrus.FieldLogger {
	return middleware.GetLogEntry(r).(*StructuredLoggerEntry).Logger
}

// logEntrySetField sets a field on a `StructuredLoggerEntry`.
func logEntrySetField(req *http.Request, key string, value interface{}) {
	logEntrySetCtxField(req.Context(), key, value)
}

// logEntrySetFields sets multiple fields on a `StructuredLoggerEntry`.
func logEntrySetFields(req *http.Request, fields map[string]interface{}) {
	logEntrySetCtxFields(req.Context(), fields)
}

// logEntrySetCtxFields set a field on a `StructuredLoggerEntry` using a `http.Request`'s
// `context.Context`.
func logEntrySetCtxField(ctx context.Context, key string, value interface{}) {
	if entry, ok := ctx.Value(middleware.LogEntryCtxKey).(*StructuredLoggerEntry); ok {
		entry.Logger = entry.Logger.WithField(key, value)
	}
}

// logEntrySetCtxFields sets multiple fields on a `StructuredLoggerEntry` using a `http.Request`'s
// `context.Context`.
func logEntrySetCtxFields(ctx context.Context, fields map[string]interface{}) {
	if entry, ok := ctx.Value(middleware.LogEntryCtxKey).(*StructuredLoggerEntry); ok {
		entry.Logger = entry.Logger.WithFields(fields)
	}
}

// REF: `defaults.Logger`

// newLogWrap creates a new `LogWrap`.
func newLogWrapper(l *logrus.Logger) LogWrapper { return LogWrapper{logger: l} }

// Info wraps `logrus.Logger`'s `Info()`.
func (w LogWrapper) Info(s string) {
	w.logger.WithField("context", internalOpContextKey).Info(s)
}

// Error wraps `logrus.Logger`'s `Error()`.
func (w LogWrapper) Error(s string) {
	w.logger.WithField("context", internalOpContextKey).Error(s)
}

// InfoWithField wraps `logrus.Logger`'s `Info()` with the `WithFields` functionality.
func (w LogWrapper) InfoWithField(field string, msg interface{}) {
	w.logger.WithFields(logrus.Fields{
		"context": internalOpContextKey,
		field:     msg,
	}).Infoln()
}

// ErrWithField wraps `logrus.Logger`'s `Error()` with the `WithFields` functionality.
func (w LogWrapper) ErrWithField(field string, err error) {
	w.logger.WithFields(logrus.Fields{
		"context": internalOpContextKey,
		field:     err,
	}).Errorln()
}
