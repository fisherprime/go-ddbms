package ddbms

import (
	"context"
	"fmt"

	"github.com/fsnotify/fsnotify"
	"github.com/ory/viper"
	log "github.com/sirupsen/logrus"
)

var (
	Debug bool
)

func monitorConfig(ctx context.Context) (err error) {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetConfigType("json")

	if err = viper.ReadInConfig(); err != nil {
		err = fmt.Errorf("could not read the config file: %w", err)
		return
	}

	viper.SetDefault("Debug", true)

	viper.SetDefault("db1dbms", "")
	viper.SetDefault("db1host", "")
	viper.SetDefault("db1name", "")
	viper.SetDefault("db1pass", "")
	viper.SetDefault("db1user", "")

	viper.SetDefault("db2dbms", "")
	viper.SetDefault("db2host", "")
	viper.SetDefault("db2name", "")
	viper.SetDefault("db2pass", "")
	viper.SetDefault("db2user", "")

	viper.SetDefault("db3dbms", "")
	viper.SetDefault("db3host", "")
	viper.SetDefault("db3name", "")
	viper.SetDefault("db3pass", "")
	viper.SetDefault("db3user", "")

	updateRuntimeConfig(ctx)

	go func() {
		viper.WatchConfig()
		viper.OnConfigChange(func(e fsnotify.Event) {
			log.Info("Config file changed:", e.Name)
			updateRuntimeConfig(ctx)
		})
	}()

	return
}

func updateRuntimeConfig(_ context.Context) {
	DB1.dbms = viper.GetString("db1dbms")
	DB1.host = viper.GetString("db1host")
	DB1.name = viper.GetString("db1name")
	DB1.pass = viper.GetString("db1pass")
	DB1.user = viper.GetString("db1user")

	DB2.dbms = viper.GetString("db2dbms")
	DB2.host = viper.GetString("db2host")
	DB2.name = viper.GetString("db2name")
	DB2.pass = viper.GetString("db2pass")
	DB2.user = viper.GetString("db2user")

	DB3.dbms = viper.GetString("db3dbms")
	DB3.host = viper.GetString("db3host")
	DB3.name = viper.GetString("db3name")
	DB3.pass = viper.GetString("db3pass")
	DB3.user = viper.GetString("db3user")

	log.Info("Read system config file")
}

func modifyConfig(ctx context.Context) (err error) {
	viper.Set("Debug", Debug)

	viper.Set("db1dbms", DB1.dbms)
	viper.Set("db1host", DB1.host)
	viper.Set("db1name", DB1.name)
	viper.Set("db1pass", DB1.pass)
	viper.Set("db1user", DB1.user)

	viper.Set("db2dbms", DB2.dbms)
	viper.Set("db2host", DB2.host)
	viper.Set("db2name", DB2.name)
	viper.Set("db2pass", DB2.pass)
	viper.Set("db2user", DB2.user)

	viper.Set("db3dbms", DB3.dbms)
	viper.Set("db3host", DB3.host)
	viper.Set("db3name", DB3.name)
	viper.Set("db3pass", DB3.pass)
	viper.Set("db3user", DB3.user)

	if err = viper.WriteConfig(); err != nil {
		return
	}
	log.Info("Updated system config file")

	return
}
