package ddbms

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"regexp"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jackc/pgx"
	_ "modernc.org/sqlite"
)

type (
	StringSlice []string

	// FragmentSchema contains the informaion on a fragment.
	FragmentInfo struct {
		// The filters field is informative, not used in any computation
		filters     [][]string
		fKeys       string
		globalTable string
		name        string
		pKey        string
		projection  string
		size        int
	}

	// DBInfo contains the information on a database.
	DBInfo struct {
		conn      *sql.DB
		fragments []*FragmentInfo
		dbms      string
		host      string
		name      string
		pass      string
		user      string
	}
)

var (
	// Loaded from the config.
	DB1 DBInfo
	DB2 DBInfo
	DB3 DBInfo

	dbDetailsMap map[string]*DBInfo

	memoryDB *sql.DB

	errEmptyResp = errors.New("Empty response received")
)

func loadDBDetails() {
	dbDetailsMap = map[string]*DBInfo{
		DB1.name: &DB1,
		DB2.name: &DB2,
		DB3.name: &DB3,
	}

	loadFragmentSchema()
}

func loadFragmentSchema() {
	// TODO: Move to a config / load from the DB, the filter will be tricky to read.
	deptsFrag1 := FragmentInfo{
		filters:     [][]string{{""}},
		fKeys:       "",
		globalTable: "departments",
		name:        "departments1",
		pKey:        "dept_no",
	}

	deptManagerFrag1 := FragmentInfo{
		filters:     [][]string{{""}},
		fKeys:       "dept_no",
		globalTable: "dept_manager",
		name:        "dept_manager1",
		pKey:        "emp_no",
	}

	deptManagerFrag2 := FragmentInfo{
		filters:     [][]string{{""}},
		fKeys:       "",
		globalTable: "dept_manager",
		name:        "dept_manager2",
		pKey:        "emp_no",
	}

	deptEmpFrag1 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "41662"}},
		fKeys:       "dept_no",
		globalTable: "dept_emp",
		name:        "dept_emp1",
		pKey:        "emp_no",
	}

	deptEmpFrag2 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "73336"}},
		fKeys:       "dept_no",
		globalTable: "dept_emp",
		name:        "dept_emp2",
		pKey:        "emp_no",
	}

	deptEmpFrag3 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "105008"}},
		fKeys:       "dept_no",
		globalTable: "dept_emp",
		name:        "dept_emp3",
		pKey:        "emp_no",
	}

	deptEmpFrag4 := FragmentInfo{
		filters:     [][]string{{"emp_no", ">", "107007"}},
		fKeys:       "dept_no",
		globalTable: "dept_emp",
		name:        "dept_emp4",
		pKey:        "emp_no",
	}

	salariesFrag1 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "31030"}},
		fKeys:       "emp_no",
		globalTable: "salaries",
		name:        "salaries1",
		pKey:        "emp_no",
	}

	salariesFrag2 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "52137"}},
		fKeys:       "emp_no",
		globalTable: "salaries",
		name:        "salaries2",
		pKey:        "emp_no",
	}

	salariesFrag3 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "73199"}},
		fKeys:       "emp_no",
		globalTable: "salaries",
		name:        "salaries3",
		pKey:        "emp_no",
	}

	salariesFrag4 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "94198"}},
		fKeys:       "emp_no",
		globalTable: "salaries",
		name:        "salaries4",
		pKey:        "emp_no",
	}

	salariesFrag5 := FragmentInfo{
		filters:     [][]string{{"emp_no", ">", "94197"}},
		fKeys:       "emp_no",
		globalTable: "salaries",
		name:        "salaries5",
		pKey:        "emp_no",
	}

	empFrag1 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "45002"}},
		fKeys:       "",
		globalTable: "employees",
		name:        "employees1",
		pKey:        "emp_no",
	}

	empFrag2 := FragmentInfo{
		filters:     [][]string{{"emp_no", "<", "80004"}},
		fKeys:       "",
		globalTable: "employees",
		name:        "employees2",
		pKey:        "emp_no",
	}

	empFrag3 := FragmentInfo{
		filters:     [][]string{{"emp_no", ">", "80003"}},
		fKeys:       "",
		globalTable: "employees",
		name:        "employees3",
		pKey:        "emp_no",
	}

	DB1.fragments = []*FragmentInfo{
		&deptsFrag1,
		&deptManagerFrag1,
		&deptEmpFrag1,
		&deptEmpFrag2,
		&deptEmpFrag3,
		&deptEmpFrag4,
	}
	DB2.fragments = []*FragmentInfo{
		&deptManagerFrag2,
		&salariesFrag1,
		&salariesFrag2,
		&salariesFrag3,
		&salariesFrag4,
		&salariesFrag5,
	}
	DB3.fragments = []*FragmentInfo{
		&empFrag1,
		&empFrag2,
		&empFrag3,
	}
}

func connectDB(ctx context.Context, db *DBInfo) (err error) {
	// Driver name & data source name.
	var driverName, dsn string

	switch (*db).dbms {
	case "postgresql":
		splitHost := strings.Split((*db).host, ":")

		driverName = "postgres"
		dsn = fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
			splitHost[0], splitHost[1], (*db).name, (*db).user, (*db).pass)
	case "cockroachdb":
		splitHost := strings.Split((*db).host, ":")

		driverName = "postgres"
		dsn = fmt.Sprintf("host=%s port=%s dbname=%s user=%s sslmode=disable", splitHost[0],
			splitHost[1], (*db).name, (*db).user)
	case "mariadb", "mysql":
		driverName = "mysql"
		dsn = fmt.Sprintf("%s:%s@tcp(%s)/%s", (*db).user, (*db).pass, (*db).host,
			(*db).name)
	default:
		err = fmt.Errorf("unsupported DBMS: %s", (*db).dbms)
		return
	}

	if (*db).conn, err = sql.Open(driverName, dsn); err != nil {
		return
	}
	if err = db.conn.PingContext(ctx); err != nil {
		return
	}

	// FIXME: query the information_schema rather than manually define things / perform
	// multiple queries.
	if (*db).conn != nil {
		for _, fragment := range (*db).fragments {
			// Update fragment projection
			var rows *sql.Rows
			if rows, err = (*db).conn.Query(fmt.Sprintf("select * from %s limit 1;", fragment.name)); err != nil {
				err = fmt.Errorf("%s: %w", (*db).name, err)
				return
			}

			// If columns exist.
			if cols, _ := rows.Columns(); len(cols) > 0 {
				fragment.projection = strings.Join(cols, ", ")
				queryAttribute := strings.Split(fragment.projection, ",")[0]

				// Update fragment size
				if err = (*db).conn.QueryRow(fmt.Sprintf("select count(%s) from %s;",
					queryAttribute, fragment.name)).Scan(&fragment.size); err != nil {
					err = fmt.Errorf("%s: %w", (*db).name, err)
					return
				}
				logger.Debugf("Fragment (%s): %v\n", fragment.name, fragment)
			}
		}
	}

	if err = (*db).conn.Ping(); err != nil {
		err = fmt.Errorf("%s: %w", (*db).name, err)
		return
	}

	logger.Info("Connected to: ", (*db).dbms)

	return
}

// REF: https://stackoverflow.com/questions/52895102/bulk-insert-copy-sql-table-with-golang
// Additional reference: https://github.com/go-sql-driver/mysql/wiki/Examples
func populateTempTable(tx *sql.Tx, rows *sql.Rows, tempTableName string) (err error) {
	var placeholderSlice []string

	columns, _ := rows.Columns()
	numColumns := len(columns)

	bulkValues := []interface{}{}

	rawScanners := make([]interface{}, numColumns)
	values := make([]sql.RawBytes, numColumns)

	for index := 0; index < numColumns; index++ {
		rawScanners[index] = &values[index]
	}

	placeholderStr := "(" + strings.Repeat("?,", (numColumns-1)) + "?)"

	for rows.Next() {
		rows.Scan(rawScanners...)

		record := make([]interface{}, numColumns)

		for index, value := range values {
			if value == nil {
				record[index] = "NULL"
				continue
			}

			record[index] = string(value)
			logger.Debug(columns[index], ": ", record[index])
		}

		placeholderSlice = append(placeholderSlice, placeholderStr)
		bulkValues = append(bulkValues, record...)
	}

	/* queryStr := fmt.Sprintf("INSERT INTO %s VALUES %s", tempTableName, strings.Join(placeholderSlice, ","))
	 * _, err := db.conn.Exec(queryStr, bulkValues...)
	 * if err != nil {
	 *     logger.Println(fmt.Sprintf("[!] Couldn't populate temporary table(%s): %s", db.name, err.Error()))
	 * } */

	dataSize := len(bulkValues)
	stdDataSize := 0

	if dataSize > 0 {
		stdDataSize = dataSize / numColumns
	}
	chunkSize := 10000

	for pos := 0; pos < stdDataSize; pos += chunkSize {
		limit := pos + chunkSize - 1

		if limit >= stdDataSize {
			limit = stdDataSize - 1
		}

		logger.Debug("len:", limit-pos, "len:", (limit*numColumns)-(pos*numColumns))
		logger.Debug("placeholder:", placeholderSlice[0], "values:", bulkValues[0:numColumns])
		logger.Debug(bulkValues[(pos * numColumns):(limit * numColumns)])

		queryStr := fmt.Sprintf("insert into %s values %s;", tempTableName,
			strings.Join(placeholderSlice[pos:limit], ","))
		tag, err := tx.Exec(queryStr, bulkValues[(pos*numColumns):(limit*numColumns)]...)
		if err != nil {
			logger.WithError(err).Error(fmt.Sprintf("failed to populate temporary table(%s)", tempTableName))
			continue
		}

		affectedRows, _ := tag.RowsAffected()
		logger.Debugf("Created %d rows\n", affectedRows)
	}

	return
}

// Merges N select queries.
//
// Operation is messy & unsafe.
func joinSelectQueries(queries []string) string {
	var (
		finalQuery string

		lenSelection []int

		selection, cartProd, filter StringSlice

		redundantAttribs []StringSlice
	)

	for _, query := range queries {
		/* identifierRegx := `\(([a-Z_.[0-9]],*)+\)`
		 * rawRegx := `\(select\)\ +` + identifierRegx + `\ +from\ +` + identifierRegx + `\ +where\ +` + `.*$`
		 * regx := regexp.MustCompile(rawRegx) */

		selectRegx := regexp.MustCompile(`select\ +`)
		fromRegx := regexp.MustCompile(`\ +from\ +`)
		whereRegex := regexp.MustCompile(`\ +where\ +`)
		stmtEndRegex := regexp.MustCompile(`;`)

		selectIndices := selectRegx.FindIndex([]byte(query))
		fromIndices := fromRegx.FindIndex([]byte(query))
		whereIndices := whereRegex.FindIndex([]byte(query))
		stmtEndIndices := stmtEndRegex.FindIndex([]byte(query))

		selectionString := query[selectIndices[1]:fromIndices[0]]
		lenSelection = append(lenSelection, len(strings.Split(selectionString, ",")))

		redundantAttribs = append(redundantAttribs, selection.UniqueAppend(selectionString))
		if whereIndices != nil {
			cartProd.UniqueAppend(query[fromIndices[1]:whereIndices[0]])
			filter.UniqueAppend(query[whereIndices[1]:stmtEndIndices[0]])

			continue
		}

		cartProd.UniqueAppend(query[fromIndices[1]:stmtEndIndices[0]])
	}

	logger.Debug("Selection lengths:", lenSelection, "\n", "Redundant: columns", redundantAttribs)

	if lenSelection[0] == lenSelection[1] &&
		lenSelection[1] == len(redundantAttribs[1]) {
		finalQuery = fmt.Sprintf("select %s from %s union %s ", strings.Join(selection, ","),
			cartProd[0], fmt.Sprintf("select %s from %s", strings.Join(selection, ","),
				cartProd[1]))
	} else {
		finalQuery = fmt.Sprintf("select %s from %s ", strings.Join(selection, ","),
			strings.Join(cartProd, ","))
	}

	if len(filter) > 0 {
		finalQuery += fmt.Sprintf(" where %s", strings.Join(filter, ","))
	}
	finalQuery += ";"

	return finalQuery
}

func (sl *StringSlice) UniqueAppend(input string) (redundant StringSlice) {
	redundant = make([]string, 0)

	logger.Debug(input)
	slice := strings.Split(input, ",")

	for _, a := range slice {
		a = strings.Trim(a, " ")
		if !sliceContains(*sl, a) {
			*sl = append(*sl, a)
			continue
		}

		redundant = append(redundant, a)
	}

	return
}
