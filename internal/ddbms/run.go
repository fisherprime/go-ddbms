package ddbms

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/sirupsen/logrus"
)

var (
	logger    *logrus.Logger
	eHandler  *ErrorHandler
	responder *Responder
)

func StartServer() (err error) {
	ctx := context.Background()

	if err = monitorConfig(ctx); err != nil {
		return
	}

	if err = initDBMS(ctx); err != nil {
		return
	}

	logger = logrus.New()
	eHandler = newErrorHandler(logger)
	responder = newResponder()

	router := chi.NewRouter()
	router.Use(
		newStructuredLogger(logger),
		middleware.Recoverer,
		middleware.SetHeader("Content-Type", "application/json"),
	)

	router.Post("/api/query", eHandler.Wrap(queryHandler))

	srv := &http.Server{
		Addr:    "127.0.2.2:2000",
		Handler: router,
	}

	idleConnsClosed := make(chan struct{})
	shutdownService(idleConnsClosed, srv)

	if err = srv.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
		return
	}
	err = nil

	<-idleConnsClosed

	return
}

// shutdownService allows for graceful shutdown of the HTTP service.
func shutdownService(idleConnsClosed chan struct{}, srv *http.Server) {
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	<-sigint

	// We received an interrupt signal, shut
	// down.
	if err := srv.Shutdown(context.Background()); err != nil {
		// Error from closing listeners, or
		// context timeout:
		log.Printf("HTTP service Shutdown: %v", err)
	}
	close(idleConnsClosed)
}

func initDBMS(ctx context.Context) (err error) {
	var wg sync.WaitGroup

	loadDBDetails()

	wg.Add(len(dbDetailsMap))

	for _, db := range dbDetailsMap {
		go func(ctx context.Context, db *DBInfo) {
			if err = connectDB(ctx, db); err != nil {
				// Execution should stop if a database connection isn't active.
				log.Fatal()
			}
			wg.Done()
		}(ctx, db)
	}
	wg.Wait()

	if memoryDB, err = sql.Open("sqlite", ":memory:"); err != nil {
		return
	}

	err = modifyConfig(ctx)

	return
}
