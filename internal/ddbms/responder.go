package ddbms

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type (
	Responder struct{}
	HTMLData  map[string]interface{}
)

const (
	errKey  = "error"
	dataKey = "data"

	errFmt = `{"status": "error": "error": "%v"}`
)

func newResponder() *Responder { return &Responder{} }

func (r *Responder) Respond(wr http.ResponseWriter, req *http.Request, code int, data HTMLData) {
	if code < http.StatusContinue || code > http.StatusNetworkAuthenticationRequired {
		code = http.StatusInternalServerError
	}

	var (
		output []byte
		err    error
	)
	if output, err = json.Marshal(data); err != nil {
		wr.WriteHeader(http.StatusInternalServerError)
		_, _ = fmt.Fprintf(wr, errFmt, err)

		return
	}

	_, err = wr.Write(output)
}
