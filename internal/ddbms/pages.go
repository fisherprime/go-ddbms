package ddbms

import (
	"errors"
	"net/http"
)

type (
	Response struct {
		data string
		err  error
	}
)

const (
	respChanLen = 10
)

var (
	errEmptyQueryResponse = errors.New("the query string is empty")
	errUnsupportedQuery   = errors.New("unsupported query type")
)

func queryHandler(wr http.ResponseWriter, req *http.Request) (err error) {
	if err = req.ParseForm(); err != nil {
		return
	}

	// A size >1 allows for buffering.
	q := &Query{
		ctx:   req.Context(),
		query: req.PostForm.Get("query"),
		resp:  make(chan Response, respChanLen),
	}

	defer close(q.resp)

	if q.query == "" {
		logger.Error(errEmptyQueryResponse)
		responder.Respond(wr, req, http.StatusBadRequest, HTMLData{errKey: errEmptyQueryResponse})

		return
	}

	go q.Execute()
	resp := <-q.resp

	if resp.err != nil {
		responder.Respond(wr, req, http.StatusBadRequest, HTMLData{errKey: err})
		return
	}

	responder.Respond(wr, req, http.StatusOK, HTMLData{dataKey: resp.data})

	return
}
