package ddbms

// REF: sourced from https://stackoverflow.com/questions/42774467/how-to-convert-sql-rows-to-typed-json-in-golang

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"reflect"

	"github.com/go-sql-driver/mysql"
)

type (
	jsonNullInt64 struct{ sql.NullInt64 }

	jsonNullFloat64 struct{ sql.NullFloat64 }

	jsonNullTime struct{ mysql.NullTime }
)

var (
	jsonNullFloat64Type = reflect.TypeOf(jsonNullFloat64{})
	jsonNullInt64Type   = reflect.TypeOf(jsonNullInt64{})
	jsonNullTimeType    = reflect.TypeOf(jsonNullTime{})
	nullFloat64Type     = reflect.TypeOf(sql.NullFloat64{})
	nullInt64Type       = reflect.TypeOf(sql.NullInt64{})
	nullTimeType        = reflect.TypeOf(mysql.NullTime{})
)

func (v jsonNullInt64) MarshalJSON() ([]byte, error) {
	if !v.Valid {
		return json.Marshal(nil)
	}
	return json.Marshal(v.Int64)
}

func (v jsonNullFloat64) MarshalJSON() ([]byte, error) {
	if !v.Valid {
		return json.Marshal(nil)
	}
	return json.Marshal(v.Float64)
}

func (v jsonNullTime) MarshalJSON() ([]byte, error) {
	if !v.Valid {
		return json.Marshal(nil)
	}
	return json.Marshal(v.Time)
}

// SQLToJSON takes an SQL result and converts it to a nice JSON form. It also
// handles possibly-null values nicely. See https://stackoverflow.com/a/52572145/265521
func SQLToJSON(rows *sql.Rows) (b []byte, err error) {
	var columns []string
	if columns, err = rows.Columns(); err != nil {
		err = fmt.Errorf("column error: %v", err)
		return
	}

	var tt []*sql.ColumnType
	if tt, err = rows.ColumnTypes(); err != nil {
		err = fmt.Errorf("column type error: %v", err)
		return
	}

	types := make([]reflect.Type, len(tt))
	for i, tp := range tt {
		st := tp.ScanType()
		switch st {
		case nil:
			err = fmt.Errorf("scantype is null for column: %v", err)
			return
		case nullInt64Type:
			types[i] = jsonNullInt64Type
		case nullFloat64Type:
			types[i] = jsonNullFloat64Type
		case nullTimeType:
			types[i] = jsonNullTimeType
		default:
			types[i] = st
		}
	}

	values := make([]interface{}, len(tt))
	data := make(map[string][]interface{})

	for rows.Next() {
		for i := range values {
			values[i] = reflect.New(types[i]).Interface()
		}
		if err = rows.Scan(values...); err != nil {
			err = fmt.Errorf("failed to scan values: %v", err)
			return
		}
		for i, v := range values {
			data[columns[i]] = append(data[columns[i]], v)
		}
	}

	return json.Marshal(data)
}
