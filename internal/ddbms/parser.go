package ddbms

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

// NOTE: This file has jank code; should be replaced with an AST implementation.

const (
	querySelect = selectK
	queryUpdate = updateK

	lexTimeout = 10 * time.Second
)

var (
	errUnterminatedQuery = errors.New("the query is not terminated")
	errUnknownToken      = errors.New("unknown token type")
)

func (q *Query) Parse() (executionOrder []*SiteDetails, err error) {
	lowerQuery := strings.ToLower(q.query)

	switch {
	case strings.Contains(lowerQuery, selectK):
		q.stmtType = querySelect
	case strings.Contains(lowerQuery, updateK):
		q.stmtType = queryUpdate
	default:
		err = errUnsupportedQuery
		return
	}

	logger.Info("Begin parsing")
	lexer := newLexer(q.ctx, q.query)
	go lexer.Lex()

	item, proceed := <-lexer.item
	if item.err != nil {
		err = item.err
		return
	}
	if !proceed {
		return
	}
	logger.Debugf("%s: (%s)\n", itemNames[item.id], item.tok)
	q.parseTree = newTree(q.ctx, item.tok)
	parseTree := q.parseTree

	counter := -1
	for {
		counter++

		item, proceed := <-lexer.item
		if item.err != nil {
			err = item.err
			return
		}
		if !proceed {
			break
		}

		logger.Debugf("%s: (%s)\n", itemNames[item.id], item.tok)
		switch item.id {
		case ItemSemi:
			break
		case ItemEOF:
			err = errUnterminatedQuery
			return
		case ItemWhitespace:
			continue
		case ItemKeyword:
			parseTree = q.parseTree
			node := newTree(q.ctx, item.tok)
			if err = parseTree.AddOperand(item.tok); err != nil {
				return
			}
			parseTree = node
		default:
			if err = parseTree.AddOperand(item.tok); err != nil {
				return
			}
		}
	}

	if err = q.PopulateSections(); err != nil {
		return
	}
	if err = q.Reduce(); err != nil {
		return
	}

	if Debug {
		for _, q := range q.fragmentQueries {
			logger.Debug("Fragment query: ", q)
		}
	}

	if q.stmtType == querySelect {
		if executionOrder, err = q.Optimize(); err != nil {
			return
		}

		if Debug {
			logExecutionOrder(&executionOrder)
		}
	}

	return
}

func logExecutionOrder(executionOrder *[]*SiteDetails) {
	s := strings.Builder{}

	splitLim := len(*executionOrder) - 1
	for k, node := range *executionOrder {
		fmt.Fprintf(&s, "%s(%d)", node.name, node.size)
		if k < splitLim {
			fmt.Fprint(&s, "->")
		}
	}
	logger.Debugf("Execution order: [%s]\n", s.String())
	s.Reset()
}
