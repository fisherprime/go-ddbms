package ddbms

import (
	"errors"
	"fmt"

	"strings"
	"time"
)

type (
	Filter struct {
		comparator    *string
		compareFilter *bool
		counter       *int
		filter        *[]string
		filterDigit   *bool
		filterRHSD    *float64
		filterRHST    *time.Time
		filterTime    *bool
		keyword       *string
		lhs           *string
		rhs           *string
		rhsDigit      *float64
		rhsTime       *time.Time
		selection     *[]string
		tempQuery     *[]string
	}
)

var (
	errFragmentsMissing = errors.New("Requested table(s) (& fragment(s)) do not exist")
)

func (q *Query) Reduce() (err error) {
	logger.Info("Begin reduction")

	switch q.stmtType {
	case querySelect:
		logger.Debug("Projection: ", q.targetList, "\n", "Cartesian product: ", q.fromClause,
			"\n", "Selection: ", q.whereClause)
		return q.ReduceSelect()
	case queryUpdate:
		logger.Debug("Tables: ", q.fromClause, "\n", "Values: ", q.valuesList, "\n", "Filters: ", q.whereClause)
		return q.ReduceUpdate()
	default:
	}

	return
}

// FIXME: account for null results on cartesian product (section) fragments
func (q *Query) ReduceSelect() (err error) {
	var validQueries int

	logger.Debug("SELECT reducer")

	// Iterate over databases
	for _, db := range dbDetailsMap {
		// Iterate over fragments of the database
	FragmentLoop:
		for _, fragment := range db.fragments {
			var (
				counter, validSelections int

				isReference bool

				keyword       string
				pS, tempQuery []string
			)

			for _, table := range q.fromClause {
				if fragment.globalTable == table {
					isReference = true
				}
			}

			if !isReference {
				continue
			}

			for _, field := range q.targetList {
				projectionComponents := strings.Split(field, ".")
				lenComponents := len(projectionComponents)
				projectionAttribute := projectionComponents[lenComponents-1]

				// Match projection schema to current fragment's schema.
				if lenComponents == 2 {
					if projectionComponents[0] != fragment.globalTable {
						continue
					}
				}
				logger.Debug("DB:", db.name, ", fragment:", fragment.name, ", projection:", field)

				// Use all fields
				if projectionAttribute == "*" {
					// pS = make([]string, 0)
					for _, a := range strings.Split(fragment.projection, ",") {
						if !sliceContains(pS, a) {
							pS = append(pS, a)
						}
					}

					continue
				}

				// Leave foreign keys out of a fragment's query
				/* if strings.Contains(fragment.projection, p) &&
				 *     !strings.Contains(fragment.foreignKeys, p) &&
				 *     !Contains(pS, p) {
				 *     pS = append(pS, p)
				 * } */

				if strings.Contains(fragment.projection, projectionAttribute) &&
					!sliceContains(pS, projectionAttribute) {
					pS = append(pS, projectionAttribute)
				}
			}

			if len(pS) < 1 {
				continue
			}

			tempQuery = []string{
				"select ",
				strings.Join(pS, ", "),
				" from ",
				fragment.name}

			// Iterate over where clauses
			for _, selection := range q.whereClause {
				var (
					// filterDigit bool
					// filterTime  bool

					// comparator string
					lhs string
					// rhs        string

					// rhsDigit float64
					// rhsTime  time.Time
				)

				lhs = selection[0]
				if len(selection) == 3 {
					comparator := selection[1]
					rhs := selection[2]

					pSlice := strings.Split(lhs, ".")
					p := pSlice[len(pSlice)-1]

					if len(pSlice) == 3 {
						if pSlice[0] != fragment.globalTable {
							continue
						}
					}

					// If filter column is defined for this fragment
					if strings.Contains(fragment.projection, p) {
						var (
							// compareFilter bool
							// filterRHSD    float64
							// filterRHST    time.Time
							numRecords   int
							whereSection = strings.Join([]string{p, comparator, rhs},
								"")
						)

						qStr := fmt.Sprintf("select count(%s) from %s where %s;",
							p, fragment.name, whereSection)
						err := db.conn.QueryRow(qStr).Scan(&numRecords)

						// Column doesn't exist
						if err != nil {
							continue
						}

						validSelections++

						// Null fragment response
						if numRecords < 1 {
							continue FragmentLoop
						}

						if counter == 0 {
							tempQuery = append(tempQuery, "where")
						}

						if keyword != "" {
							tempQuery = append(tempQuery, keyword)
							keyword = ""
						}

						counter++
						tempQuery = append(tempQuery, whereSection)

						/* for _, fragmentFilter := range fragment.filters {
						 *     parseFilter(FilterVals{
						 *         comparator:    &comparator,
						 *         compareFilter: &compareFilter,
						 *         counter:       &counter,
						 *         filter:        &fragmentFilter,
						 *         filterDigit:   &filterDigit,
						 *         filterRHSD:    &filterRHSD,
						 *         filterRHST:    &filterRHST,
						 *         filterTime:    &filterTime,
						 *         keyword:       &keyword,
						 *         lhs:           &lhs,
						 *         rhs:           &rhs,
						 *         rhsDigit:      &rhsDigit,
						 *         rhsTime:       &rhsTime,
						 *         selection:     &selection,
						 *         tempQuery:     &tempQuery,
						 *     })
						 * } // End for */
					} // End if
				} // End if

				if lhs != " " {
					keyword = lhs
				}
			} // End for (selection)

			if len(tempQuery) < (4 + validSelections) {
				break
			}
			logger.Debug("Query:", tempQuery, ",length:", len(tempQuery), ",valid selections:",
				validSelections)

			queryString := strings.Join(tempQuery, " ") + ";"
			q.fragmentQueries = append(q.fragmentQueries, queryString)
			validQueries++
		} // End for (fragment)
	} // End for (db)

	// time.Sleep(5 * time.Second)
	if validQueries < 1 {
		err = errFragmentsMissing
		return
	}

	return
}

func (q *Query) ReduceUpdate() (err error) {
	var validQueries int

	logger.Debug("UPDATE reducer")

	for _, db := range dbDetailsMap {
		for _, fragment := range db.fragments {
			var (
				counter     int
				validFilter int

				isRef     bool
				keyword   string
				vS        []string
				tempQuery []string
			)

			for _, table := range q.fromClause {
				if fragment.globalTable == table {
					isRef = true
				}
			}

			if !isRef {
				continue
			}

			tempQuery = append(tempQuery, "update", fragment.name)

			for _, value := range q.valuesList {
				var (
					comparator string
					lhs        string
					rhs        string
				)

				vSlice := strings.Split(value[0], ".")
				lhs = vSlice[len(vSlice)-1]

				if len(value) == 3 {
					comparator = value[1]
					rhs = value[2]

					if strings.Contains(fragment.projection, lhs) &&
						!strings.Contains(fragment.fKeys, lhs) &&
						!sliceContains(vS, lhs) {

						// If filter column is defined for this fragment
						if rhs != "" && comparator == "=" {
							if counter == 0 {
								tempQuery = append(tempQuery, "set")
							}

							if keyword != "" {
								vS = append(vS, keyword)
								keyword = ""

								break
							}

							counter++
							vS = append(vS, string(lhs+comparator+rhs))
						} // End if
					} // End if

					continue
				} // End if

				if lhs != " " {
					keyword = lhs
				}
			} // End for

			if len(vS) < 1 {
				continue
			}

			tempQuery = append(tempQuery, strings.Join(vS, ","))
			counter = 0

			// Iterate over where clauses
			for _, filter := range q.whereClause {
				var lhs string

				lhs = filter[0]
				if len(filter) == 3 {
					comparator := filter[1]
					rhs := filter[2]

					pSlice := strings.Split(lhs, ".")
					p := pSlice[len(pSlice)-1]

					if len(pSlice) == 3 {
						if pSlice[0] != fragment.globalTable {
							continue
						}
					}

					// If filter column is defined for this fragment
					if strings.Contains(fragment.projection, lhs) {
						var (
							numRecords    int
							filterSection = strings.Join([]string{p, comparator, rhs},
								"")
						)

						qStr := fmt.Sprintf("select count(%s) from %s where %s;",
							p, fragment.name, filterSection)
						err := db.conn.QueryRow(qStr).Scan(&numRecords)

						if err != nil {
							continue
						}

						validFilter++

						// Null fragment response
						if numRecords < 1 {
							continue
						}

						if counter == 0 {
							tempQuery = append(tempQuery, "where")
						}

						if keyword != "" {
							tempQuery = append(tempQuery, keyword)
							keyword = ""
						}

						counter++
						tempQuery = append(tempQuery, filterSection)
					} // End if
				} // End if

				if lhs != " " {
					keyword = lhs
				}
			} // End for (selection)

			if len(tempQuery) < (4 + validFilter) {
				break
			}

			queryString := strings.Join(tempQuery, " ") + ";"
			q.fragmentQueries = append(q.fragmentQueries, queryString)
			validQueries++
		} // End for (fragment)
	} // End for (db)

	if validQueries < 1 {
		err = errFragmentsMissing
		return
	}

	return
}

/* // FIXME: update to compare filter comparators
 * // The selection isn't moved closer to the nodes in this case
 * func parseFilter(f FilterVals) {
 *     if len(*f.selection) != 3 {
 *         return
 *     }
 *
 *     if (*f.filter)[0] == *f.lhs {
 *         switch *f.comparator {
 *         case "=", ">", ">=", "<", "<=":
 *             var err error
 *
 *             *f.rhsDigit, err = strconv.ParseFloat(*f.rhs, 64)
 *             if err != nil {
 *                 if *f.rhsTime, err = time.Parse(*f.rhs, time.RFC3339); err != nil {
 *                     logger.Println("[~] Comparison on non-numeric / time value")
 *
 *                     break
 *                 }
 *             }
 *
 *             if *f.rhsDigit != 0.0 {
 *                 *f.filterDigit = true
 *
 *                 if *f.compareFilter {
 *                     if *f.filterRHSD, err = strconv.ParseFloat((*f.filter)[2], 64); err != nil {
 *                         logger.Println("[!] Comparing values of different types: float, ...")
 *
 *                         break
 *                     }
 *                 }
 *             }
 *
 *             if !(*f.rhsTime).IsZero() {
 *                 *f.filterTime = true
 *
 *                 if *f.compareFilter {
 *                     if *f.filterRHST, err = time.Parse((*f.filter)[2], time.RFC3339); err != nil {
 *                         logger.Println("[!] Comparing values of different types, time, ...")
 *
 *                         break
 *                     }
 *                 }
 *             }
 *
 *             fallthrough
 *         case ">":
 *             if *f.filterDigit {
 *                 *f.rhsDigit--
 *             }
 *
 *             if *f.filterTime {
 *                 (*f.rhsTime).Add(-1 * time.Second)
 *             }
 *
 *             fallthrough
 *         case ">=":
 *             if *f.compareFilter {
 *                 if (*f.filterDigit && *f.rhsDigit >= *f.filterRHSD) ||
 *                     (*f.filterTime && (*f.rhsTime).After(*f.filterRHST)) {
 *                     break
 *                 }
 *
 *                 return
 *             }
 *         case "<":
 *             if *f.filterDigit {
 *                 *f.rhsDigit++
 *             }
 *
 *             if *f.filterTime {
 *                 (*f.rhsTime).Add(1 * time.Second)
 *             }
 *
 *             fallthrough
 *         case "<=":
 *             if *f.compareFilter {
 *                 if (*f.filterDigit && *f.rhsDigit <= *f.filterRHSD) ||
 *                     (*f.filterTime && (*f.rhsTime).Before(*f.filterRHST)) {
 *                     break
 *                 }
 *
 *                 return
 *             }
 *         default:
 *             logger.Println("[!] Invalid where clause: ", strings.Join(*f.selection, ""))
 *
 *             return
 *         } // End switch
 *     }
 *
 *     if *f.counter == 0 {
 *         *f.tempQuery = append(*f.tempQuery, "where")
 *     }
 *
 *     if *f.keyword != "" {
 *         *f.tempQuery = append(*f.tempQuery, *f.keyword)
 *         *f.keyword = ""
 *     }
 *
 *     *f.counter++
 *     *f.tempQuery = append(*f.tempQuery, strings.Join(*f.selection, ""))
 * } */
